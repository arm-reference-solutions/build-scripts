#!/usr/bin/env bash

# Copyright (c) 2024, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

#------------------------------------------
# Generate the disk image for buidroot boot
#------------------------------------------

#variables for image generation
BLOCK_SIZE=512
SEC_PER_MB=$((1024*2))
EXT3PART_UUID=535add81-5875-4b4a-b44a-464aee5f5cbd
ROOTFS_PART_UUID=535add81-5875-4b4a-b44a-464aee5f5cbe

create_grub_cfgfiles ()
{
        local fatpart_name="$1"
        mcopy -i  $fatpart_name -o ${GRUB_FS_CONFIG_FILE} ::/grub/grub.cfg
}

create_fatpart ()
{
        local fatpart_name="$1"  #Name of the FAT partition disk image
        local fatpart_size="$2"  #FAT partition size (in 512-byte blocks)

        dd if=/dev/zero of=$fatpart_name bs=$BLOCK_SIZE count=$fatpart_size
        mkfs.vfat $fatpart_name
        mmd -i $fatpart_name ::/EFI
        mmd -i $fatpart_name ::/EFI/BOOT
        mmd -i $fatpart_name ::/grub
        mcopy -i $fatpart_name bootaa64.efi ::/EFI/BOOT
        echo "FAT partition image created"
}

create_ext3part ()
{
        local ext3part_name="$1"  #Name of the ext3 partition disk image
        local ext3part_size=$2    #ext3 partition size (in 512-byte blocks)

        dd if=/dev/zero of=$ext3part_name bs=$BLOCK_SIZE count=$ext3part_size
        mkdir -p mnt

        cp $LINUX_OUTDIR/arch/arm64/boot/Image ./mnt
        sync
        mkfs.ext3 -d mnt $ext3part_name -U $EXT3PART_UUID
        rm -rf mnt
        echo "EXT3 partition image created"
}

create_diskimage ()
{
        local image_name="$1"
        local part_start="$2"
        local fatpart_size="$3"
        local ext3part_size="$4"
        local rootfspart_size="$5"

        (echo n; echo 1; echo $part_start; echo +$((fatpart_size-1)); echo 0700; echo w; echo y) | gdisk $image_name
        (echo n; echo 2; echo $((part_start+fatpart_size)); echo +$ext3part_size; echo 8300; echo w; echo y) | gdisk $image_name
        (echo x; echo c; echo 2; echo $EXT3PART_UUID; echo w; echo y) | gdisk $image_name
        (echo n; echo 3; echo $((part_start+fatpart_size+ext3part_size)); echo +$rootfspart_size; echo 8300; echo w; echo y) | gdisk $image_name
        (echo x; echo c; echo 3; echo $ROOTFS_PART_UUID; echo w; echo y) | gdisk $image_name
}

prepare_disk_image ()
{
        echo
        echo
        echo "---------------------------------------"
        echo "Preparing disk image for debian boot"
        echo "---------------------------------------"

        pushd $GRUB_OUTDIR
        local IMG_BB=grub-debian.img
        local FAT_SIZE_MB=20
        local EXT3_SIZE_MB=200
        local ROOTFS_SIZE_B=$(ls -l $DEPLOY_DIR/$PLATFORM/$DEBIAN_IMG | awk '{print $5}')
        local ROOTFS_SIZE_MB=$(($ROOTFS_SIZE_B/(1024*1024)))
        local PART_START=$((1*SEC_PER_MB))
        local FAT_SIZE=$((FAT_SIZE_MB*SEC_PER_MB))
        local EXT3_SIZE=$((EXT3_SIZE_MB*SEC_PER_MB))
        local ROOTFS_SIZE=$((ROOTFS_SIZE_MB*SEC_PER_MB))

        ln -sf $GRUB_OUTDIR/grubaa64.efi $GRUB_OUTDIR/bootaa64.efi
        grep -q -F 'mtools_skip_check=1' ~/.mtoolsrc || echo "mtools_skip_check=1" >> ~/.mtoolsrc

        #Package images for Debian
        rm -f $IMG_BB
        dd if=/dev/zero of=part_table bs=$BLOCK_SIZE count=$PART_START

        #Space for partition table at the top
        cat part_table > $IMG_BB

        #Create fat partition
        create_fatpart "fat_part" $FAT_SIZE
        create_grub_cfgfiles "fat_part"
        cat fat_part >> $IMG_BB

        #Create ext3 partition
        create_ext3part "ext3_part" $EXT3_SIZE
        cat ext3_part >> $IMG_BB

        # create_rootfspart "rootfs_part" $ROOTFS_SIZE
        cat $DEPLOY_DIR/$PLATFORM/$DEBIAN_IMG >> $IMG_BB

        #Space for backup partition table at the bottom (1M)
        cat part_table >> $IMG_BB

        # create disk image and copy into output folder
        create_diskimage $IMG_BB $PART_START $FAT_SIZE $EXT3_SIZE $ROOTFS_SIZE

        #remove intermediate files
        rm -f part_table
        rm -f fat_part
        rm -f ext3_part

        echo "Completed preparation of disk image for debian boot"
        echo "------------------------------------------------------"
}

do_build ()
{
         info_echo "Bulding grub-debian image"
         prepare_disk_image
}

do_patch ()
{
         true
}

do_deploy ()
{
         info_echo "Deploying grub-debian image"
         cp $GRUB_OUTDIR/grub-debian.img $DEPLOY_DIR/$PLATFORM
}

do_clean ()
{
         info_echo "Cleaning grub-debian image"
         rm -rf $GRUB_OUTDIR/grub-debian.img
         rm -rf $DEPLOY_DIR/$PLATFORM/grub-debian.img
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
