#!/usr/bin/env bash

# Copyright (c) 2024, ARM Limited and Contributors. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# Redistributions of source code must retain the above copyright notice, this
# list of conditions and the following disclaimer.
#
# Redistributions in binary form must reproduce the above copyright notice,
# this list of conditions and the following disclaimer in the documentation
# and/or other materials provided with the distribution.
#
# Neither the name of ARM nor the names of its contributors may be used
# to endorse or promote products derived from this software without specific
# prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.

#
# This script uses the following environment variables from the variant
#
# UEFI_SRC - Directory containing UEFI code
# UEFI_BUILD_MODE - DEBUG or RELEASE
# UEFI_TOOLCHAIN - Toolchain supported by Linaro uefi-tools: GCC49, GCC48 or GCC47
# UEFI_PLATFORMS - Platforms to build
# UEFI_ACPICA_PATH - Path to ACPICA tools containing the iasl command

do_build ()
{
  info_echo "Building uefi"
  pushd $UEFI_ACPICA_PATH
  make iasl
  popd
  PATH=$PATH:$UEFI_ACPICA_PATH/bin
  pushd $UEFI_SRC
  CROSS_COMPILE_DIR=$(dirname $UEFI_COMPILER)
  PATH="$PATH:$CROSS_COMPILE_DIR"
  source edksetup.sh
  make -C BaseTools
  export EDK2_TOOLCHAIN=$UEFI_TOOLCHAIN
  export ${UEFI_TOOLCHAIN}_AARCH64_PREFIX=$UEFI_COMPILER
  export WORKSPACE=$UEFI_SRC
  export PACKAGES_PATH=$PWD/:$PWD/edk2-platforms
  dsc=$UEFI_SRC/edk2-platforms/Platform/ARM/Tc/${UEFI_PLATFORMS}/${UEFI_PLATFORMS}.dsc
  build -a "AARCH64" -t $UEFI_TOOLCHAIN -b $UEFI_BUILD_MODE -p ${dsc}
  mkdir -p $UEFI_OUTDIR
  cp $UEFI_SRC/Build/${UEFI_PLATFORMS}/${UEFI_BUILD_MODE}_${UEFI_TOOLCHAIN}/FV/BL33_AP_UEFI.fd $UEFI_OUTDIR/uefi.bin
  popd
}

do_clean ()
{
  info_echo "Cleaning Uefi"
  if [ -d $UEFI_SRC/Build ]; then
          pushd $UEFI_SRC
          rm -rf Build
          popd
  fi
  rm -rf $UEFI_OUTDIR
}

do_deploy ()
{
  ln -sf $UEFI_OUTDIR/uefi.bin $DEPLOY_DIR/$PLATFORM/
}

do_patch()
{
  info_echo "Patching edk2-platforms"
  PATCHES_DIR=$FILES_DIR/uefi/edk2-platforms/
  with_default_shell_opts patching $PATCHES_DIR $UEFI_SRC/edk2-platforms
}

source "$(dirname ${BASH_SOURCE[0]})/framework.sh"
