.. _docs/totalcompute/tc2/expected-test-results:

Expected test results
=====================

.. contents::


.. _docs/totalcompute/tc2/expected-test-results_acs:


ACS (UEFI boot with ACPI support) Test Suite unit tests
-------------------------------------------------------

::

	(...output truncated...)

	remove-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/Application/StallForKey/StallForKey/DEBUG/StallForKey.dll 0xF7A7B000
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestInfrastructure/SCT/Framework/Sct/DEBUG/SCT.dll 0xF7422000
	Loading driver at 0x000F7421000 EntryPoint=0x000F7427728 SCT.efi
	Load support files ...
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestInfrastructure/SCT/Drivers/TestProfile/TestProfile/DEBUG/TestProfile.dll 0xF74DA000
	Loading driver at 0x000F74D9000 EntryPoint=0x000F74DC348 TestProfile.efi
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestInfrastructure/SCT/Drivers/StandardTest/StandardTest/DEBUG/StandardTest.dll 0xF7416000
	Loading driver at 0x000F7415000 EntryPoint=0x000F741C164 StandardTest.efi
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestInfrastructure/SCT/Drivers/TestRecovery/TestRecovery/DEBUG/TestRecovery.dll 0xF74D7000
	Loading driver at 0x000F74D6000 EntryPoint=0x000F74D7784 TestRecovery.efi
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestInfrastructure/SCT/Drivers/TestLogging/TestLogging/DEBUG/TestLogging.dll 0xF740F000
	Loading driver at 0x000F740E000 EntryPoint=0x000F74121E0 TestLogging.efi
	Load proxy files ...
	Load test files ...
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestCase/UEFI/EFI/Protocol/BlockIo2/BlackBoxTest/BlockIo2BBTest/DEBUG/BlockIo2BBTest.dll 0xF73F7000
	Loading driver at 0x000F73F6000 EntryPoint=0x000F73FF984 BlockIo2BBTest.efi
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestCase/UEFI/EFI/Protocol/EraseBlock/BlackBoxTest/EraseBlockBBTest/DEBUG/EraseBlockBBTest.dll 0xF73F1000
	Loading driver at 0x000F73F0000 EntryPoint=0x000F73F2874 EraseBlockBBTest.efi
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestCase/UEFI/EFI/Protocol/UFSDeviceConfig/BlackBoxTest/UFSDeviceConfigBBTest/DEBUG/UFSDeviceConfigBBTest.dll 0xF73EC000Loading driver at 0x000F73EB000 EntryPoint=0x000F73EC7FC UFSDeviceConfigBBTest.efi
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestCase/UEFI/EFI/Protocol/ComponentName2/BlackBoxTest/ComponentName2BBTest/DEBUG/ComponentName2BBTest.dll 0xF73E2000
	Loading driver at 0x000F73E1000 EntryPoint=0x000F73E627C ComponentName2BBTest.efi
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestCase/UEFI/EFI/Protocol/IPsecConfig/BlackBoxTest/IPsecConfigBBTest/DEBUG/IPsecConfigBBTest.dll 0xF73D6000
	Loading driver at 0x000F73D5000 EntryPoint=0x000F73DA06C IPsecConfigBBTest.efi
	add-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/Build/bbrSct/DEBUG_GCC5/AARCH64/SctPkg/TestCase/UEFI/EFI/Protocol/DiskIo2/BlackBoxTest/DiskIo2BBTest/DEBUG/DiskIo2BBTest.dll 0xF73C4000
	Loading driver at 0x000F73C3000 EntryPoint=0x000F73CCE80 DiskIo2BBTest.efi

	(...output truncated...)

	Loading driver at 0x000F737A000 EntryPoint=0x000F737E178 TimeServicesBBTest.efi

	Loading driver at 0x000F70A4000 EntryPoint=0x000F70A5C18 RamDiskProtocolBBTest.efi
	Test preparing...
	  Remaining test cases: 330
	  Generic services test: PlatformSpecificElements
	  Iterations: 1/1
	------------------------------------------------------------
	Arm ACS Version: v2.0.0-BETA-0
	PlatformSpecificElements
	Revision 0x00010001
	Test Entry Point GUID: A0A8BED3-3D6F-4AD8-907A-84D52EE1543B
	Test Support Library GUIDs: 
	  1F9C2AE7-F147-4D19-A5E8-255AD005EB3E
	  832C9023-8E67-453F-83EA-DF7105FA7466
	------------------------------------------------------------
	UEFI 2.6
	Test Configuration #0
	------------------------------------------------------------
	Check the platform specific elements defined in the UEFI Spec Section 2.6.2
	------------------------------------------------------------
	Logfile: "\EFI\BOOT\bbr\SCT\Log\GenericTest\EFICompliantTest0\PlatformSpecificEl
	ements_0_0_A0A8BED3-3D6F-4AD8-907A-84D52EE1543B.log"
	Test Started: 03/13/24  06:02p
	------------------------------------------------------------
	UEFI Compliant - Console protocols must be implemented -- PASS
	8F7556C2-4665-4353-A3AF-9C005A1E63E1
	/home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/
	SctPkg/TestCase/UEFI/EFI/Generic/EfiCompliant/BlackBoxTest/EfiCompliantBBTestPla
	tform_uefi.c:1022:Text Input - Yes, Text Output - Yes, Text InputEx - Yes

	UEFI Compliant - Hii protocols must be implemented -- PASS
	B7CD2D76-EA43-4013-B7D1-59EB2EC9BF1B
	/home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/
	SctPkg/TestCase/UEFI/EFI/Generic/EfiCompliant/BlackBoxTest/EfiCompliantBBTestPla
	tform_uefi.c:1106:HiiDatabase - Yes, HiiString - Yes, HiiConfigRouting - Yes, Hi
	iConfigAccess - Yes

	UEFI Compliant - Hii protocols must be implemented -- PASS
	B7CD2D76-EA43-4013-B7D1-59EB2EC9BF1B
	/home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2-test/uefi-sct/
	SctPkg/TestCase/UEFI/EFI/Generic/EfiCompliant/BlackBoxTest/EfiCompliantBBTestPla
	tform_uefi.c:1150:HiiFont - Yes

	(...output truncated...)

		  *** Starting Wakeup semantic tests ***  

	Operating System View:
	 501 : Wake from EL1 PHY Timer Int           
		   Failed on PE -    0
		   Checkpoint --  1                           : Result:  FAIL 
	 502 : Wake from EL1 VIR Timer Int           
		   Failed on PE -    0
		   Checkpoint --  1                           : Result:  FAIL 
	 503 : Wake from EL2 PHY Timer Int           
		   Failed on PE -    0
		   Checkpoint --  1                           : Result:  FAIL 
	 504 : Wake from Watchdog WS0 Int            
		Invalid SPI interrupt ID number 30     : Result:  PASS 
	 505 : Wake from System Timer Int            : Result:  A01F9000  

		  One or more Wakeup tests failed or were skipped.

		  *** Starting Peripheral tests ***  

	Operating System View:
	 601 : USB CTRL Interface                    
		   Checkpoint --  1                           : Result:  SKIPPED 
	 602 : Check SATA CTRL Interface             
		   Checkpoint --  1                           : Result:  SKIPPED 
	 603 : Check Arm BSA UART register offsets        : Result:  PASS 
	 604 : Check Arm GENERIC UART Interrupt      
		   Test Message                               : Result:  PASS 
	 606 : 16550 compatible UART                 
		   Checkpoint --  2                           : Result:  SKIPPED 

		  One or more Peripheral tests failed or were skipped.

		  *** Starting Watchdog tests ***  

	Operating System View:
	 701 : Non Secure Watchdog Access                 : Result:  PASS 
	 702 : Check Watchdog WS0 interrupt               : Result:  PASS 

		  All Watchdog tests passed.

		  *** No ECAM region found, Skipping PCIE tests *** 

		 ------------------------------------------------------- 
		 Total Tests run  =   45  Tests Passed  =   34  Tests Failed =    7
		 ------------------------------------------------------- 

		  *** BSA tests complete. Reset the system. ***

	(...output truncated...)

.. note::
	To obtain more information on how to run this test, please refer to the :ref:`Total Compute Platform User Guide - Running sanity tests <docs/totalcompute/tc2/sanity-tests_acs>` document section.



.. _docs/totalcompute/tc2/expected-test-results_acts:


ACPI Test Suite unit tests
--------------------------

::

	# acpiview -s
			              acpiview 

	 --------------- RSDP Table --------------- 

	Address  : 0xF7870018
	Length   : 36
	  
	00000000 : 52 53 44 20 50 54 52 20 - 1B 41 52 4D 4C 54 44 02   RSD PTR .ARMLTD.
	00000010 : 00 00 00 00 24 00 00 00 - 98 FE 87 F7 00 00 00 00   ....$...........
	00000020 : C8 00 00 00                                         ....

	Table Checksum : OK

	RSDP                                 :
	  Signature                          : RSD PTR 
	  Checksum                           : 0x1B
	  Oem ID                             : ARMLTD
	  Revision                           : 2
	  RSDT Address                       : 0x0
	  Length                             : 36
	  XSDT Address                       : 0xF787FE98
	  Extended Checksum                  : 0xC8
	  Reserved                           : 0 0 0

	 --------------- XSDT Table --------------- 

	Address  : 0xF787FE98
	Length   : 92
	  
	00000000 : 58 53 44 54 5C 00 00 00 - 01 0A 41 52 4D 4C 54 44   XSDT\.....ARMLTD
	00000010 : 41 52 4D 54 43 20 20 20 - 27 07 14 20 20 20 20 20   ARMTC   '..     
	00000020 : 13 00 00 01 98 FB 87 F7 - 00 00 00 00 98 FA 87 F7   ................
	00000030 : 00 00 00 00 98 E9 87 F7 - 00 00 00 00 18 FE 87 F7   ................
	00000040 : 00 00 00 00 98 F5 87 F7 - 00 00 00 00 18 EB 87 F7   ................
	00000050 : 00 00 00 00 18 EF 87 F7 - 00 00 00 00               ............

	Table Checksum : OK

	XSDT                                 :
	  Signature                          : XSDT
	  Length                             : 92
	  Revision                           : 1
	  Checksum                           : 0xA
	  Oem ID                             : ARMLTD
	  Oem Table ID                       : ARMTC   
	  Oem Revision                       : 0x20140727
	 

	(...output truncated...)


	 --------------- SSDT Table --------------- 

	Address  : 0xF787EF18
	Length   : 403
	  
	00000000 : 53 53 44 54 93 01 00 00 - 02 C3 41 52 4D 4C 54 44   SSDT......ARMLTD
	00000010 : 41 52 4D 54 43 00 00 00 - 27 07 14 20 49 4E 54 4C   ARMTC...'.. INTL
	00000020 : 28 06 23 20 10 4E 16 5F - 53 42 5F 5B 82 41 05 43   (.# .N._SB_[.A.C
	00000030 : 4F 4D 30 08 5F 48 49 44 - 0D 41 52 4D 48 30 30 31   OM0._HID.ARMH001
	00000040 : 31 00 08 5F 43 49 44 0D - 41 52 4D 48 30 30 31 31   1.._CID.ARMH0011
	00000050 : 00 08 5F 55 49 44 00 08 - 5F 53 54 41 0A 0F 08 5F   .._UID.._STA..._
	00000060 : 43 52 53 11 1A 0A 17 86 - 09 00 01 00 00 40 2A 00   CRS..........@*.
	00000070 : 10 00 00 89 06 00 01 01 - 5F 00 00 00 79 00 5B 82   ........_...y.[.
	00000080 : 41 05 43 4F 4D 31 08 5F - 48 49 44 0D 41 52 4D 48   A.COM1._HID.ARMH
	00000090 : 30 30 31 31 00 08 5F 43 - 49 44 0D 41 52 4D 48 30   0011.._CID.ARMH0
	000000A0 : 30 31 31 00 08 5F 55 49 - 44 01 08 5F 53 54 41 0A   011.._UID.._STA.
	000000B0 : 0F 08 5F 43 52 53 11 1A - 0A 17 86 09 00 01 00 00   .._CRS..........
	000000C0 : 40 2A 00 10 00 00 89 06 - 00 01 01 00 00 00 00 79   @*.............y
	000000D0 : 00 5B 82 41 04 56 52 30 - 30 08 5F 48 49 44 0D 4C   .[.A.VR00._HID.L
	000000E0 : 4E 52 4F 30 30 30 35 00 - 08 5F 55 49 44 00 08 5F   NRO0005.._UID.._
	000000F0 : 43 43 41 01 08 5F 43 52 - 53 11 1A 0A 17 86 09 00   CCA.._CRS.......
	00000100 : 01 00 00 13 1C 00 00 01 - 00 89 06 00 01 01 EC 00   ................
	00000110 : 00 00 79 00 5B 82 41 04 - 56 52 30 31 08 5F 48 49   ..y.[.A.VR01._HI
	00000120 : 44 0D 4C 4E 52 4F 30 30 - 30 35 00 08 5F 55 49 44   D.LNRO0005.._UID
	00000130 : 01 08 5F 43 43 41 01 08 - 5F 43 52 53 11 1A 0A 17   .._CCA.._CRS....
	00000140 : 86 09 00 01 00 00 15 1C - 00 00 01 00 89 06 00 01   ................
	00000150 : 01 ED 00 00 00 79 00 5B - 82 3A 4E 45 54 30 08 5F   .....y.[.:NET0._
	00000160 : 48 49 44 0D 4C 4E 52 4F - 30 30 30 33 00 08 5F 55   HID.LNRO0003.._U
	00000170 : 49 44 00 08 5F 43 52 53 - 11 1A 0A 17 86 09 00 01   ID.._CRS........
	00000180 : 00 00 00 18 00 00 01 00 - 89 06 00 01 01 8D 00 00   ................
	00000190 : 00 79 00                                            .y.

	Table Checksum : OK

	ACPI Table Header                    :
	Signature                          : SSDT
	  Length                             : 403
	  Revision                           : 2
	  Checksum                           : 0xC3
	  Oem ID                             : ARMLTD
	  Oem Table ID                       : ARMTC   
	  Oem Revision                       : 0x20140727
	  Creator ID                         : INTL
	  Creator Revision                   : 0x20230628

	Table Statistics:
		0 Error(s)
		0 Warning(s)
	#


.. note::
	To obtain more information on how to run this sanity test, please refer to the :ref:`Total Compute Platform User Guide - Running sanity tests <docs/totalcompute/tc2/sanity-tests_acts>` document section.


--------------

*Copyright (c) 2022-2024, Arm Limited. All rights reserved.*
