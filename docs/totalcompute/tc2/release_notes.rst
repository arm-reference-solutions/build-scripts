.. _docs/totalcompute/tc2/release_notes:

Release notes - TC2-2024.02.22-LSC
==================================

.. contents::

Release tag
-----------
The manifest tag for this release is ``TC2-2024.02.22-LSC``.

Components
----------
The following is a summary of the key software features of the release:

 - BSP build supporting Debian bookworm distro;
 - Trusted firmware-A for secure boot;
 - EDK2 bootloader;
 - Hafnium for S-EL2 Secure Partition Manager core;
 - OP-TEE for Trusted Execution Environment (TEE);
 - Trusted Services (Crypto and Internal Trusted Storage);
 - System Control Processor(SCP) firmware for programming the interconnect, power control, etc;
 - Runtime Security Subsystem (RSS) firmware for providing HW RoT;

Hardware Features
-----------------
 - Booker aka CoreLink CI-700 with Memory Tagging Unit (MTU) support driver in SCP firmware;
 - GIC Clayton Initialization in Trusted Firmware-A;
 - Mali-G720 GPU;
 - Mali-D71 DPU and virtual encoder support for display on Linux;
 - MHUv2 Driver for SCP and AP communication;
 - UARTs, Timers, Flash, PIK, Clock drivers;
 - PL180 MMC;
 - DynamIQ Shared Unit (DSU) with 8 cores (1 Cortex X4 + 3 Cortex A720 + 4 Cortex A520 cores configuration);
 - RSS based on Cortex M55;
 - SCP based on Cortex M3;

Software Features
-----------------

 - Debian 12 (aka Bookworm);
 - Linux Kernel 6.1.0;
 - EDK2 v202402 (Feb 2024);
 - Trusted Firmware-A v2.9;
 - Hafnium v2.9 as Secure Partition Manager (SPM) at S-EL2;
 - OP-TEE v4.1.0 as Secure Partition at S-EL1, managed by S-EL2 SPMC (Hafnium);
 - Support for secure boot based on TBBR specification (more info available at `link <https://developer.arm.com/documentation/den0006/latest>`__);
 - System Control Processor (SCP) firmware v2.12;
 - Runtime Security Subsystem (RSS) firmware v1.8.0;
 - VirtIO to mount the Debian image in the host machine as a storage device in the FVP;
 - Trusted Services (Crypto and Internal Trusted Storage) running at S-EL0;

Platform Support
----------------
 - This software release is tested on TC2 Fast Model platform (FVP) version 11.23.28.

Tools Support
-------------
 - This software release extends docker support to Debian distro (making it supported to all TC build variants).

Known issues or Limitations
---------------------------
#. Ubuntu 22.04 is not supported in this release;
#. SVE2 (Scalable Vector Extension) feature is not supported with this release;
#. ``systemd-resolved.service`` fails multiple times before successful start, which causes delay in boot time;

    This can be avoided following one of the below workaround steps:
    
    **Method 1 - editing the Kernel command line parameters:**
    
    #. during the boot process, once the Grub screen appears (list of options that allow to choose what to boot), press ``e`` key to edit the boot command;
    #. navigate to the line with the command line parameters (usually starts with ``linux /boot/vmlinux*``), and append ``systemd.mask=systemd-resolved.service``;
    #. press ``F10`` to continue to boot with the added boot parameters.
    
    **Method 2 - make the changes to be permanent for every boot:**
    
    #. wait for the OS to fully boot, login and get to the command prompt;
    #. on the command prompt, run the following command ``vi /etc/default/grub`` to edit the Grub configuration;
    #. add the following line: ``GRUB_CMDLINE_LINUX_DEFAULT="systemd.mask=systemd-resolved.service"``;
    #. save the changes and exit the ``vi`` editor;
    #. on the command prompt, run the command ``update-grub`` to update and apply the new Grub configuration.
#. Upon the completion of running the ACS (UEFI boot with ACPI support) tests, a synchronous exception is experienced, and some dump information will be presented on the ``FVP terminal_uart1_ap`` window, similar to the following excerpt:

	::

		(...)
		Operating System View:
		 701 : Non Secure Watchdog Access                 : Result:  PASS 
		 702 : Check Watchdog WS0 interrupt               : Result:  PASS 

			  All Watchdog tests passed.

			  *** No ECAM region found, Skipping PCIE tests *** 

			 ------------------------------------------------------- 
			 Total Tests run  =   45  Tests Passed  =   34  Tests Failed =    7
			 ------------------------------------------------------- 

			  *** BSA tests complete. Reset the system. *** 

		remove-symbol-file /home/runner/work/arm-systemready/arm-systemready/SR/scripts/edk2/Build/Shell/DEBUG_GCC49/AARCH64/ShellPkg/Application/bsa-acs/uefi_app/BsaAcs/DEBUG/Bsa.dll 0xF73ED000


		Synchronous Exception at 0x00000000F73F9454

		Recursive exception occurred while dumping the CPU state
		Unhandled Exception in EL3.
		x30            = 0x000000000402cb94
		x0             = 0x0000000000000000
		x1             = 0x000000000000003a
		x2             = 0x0000000000000000
		x3             = 0x00000000820003c8
		x4             = 0x0000000000000000
		x5             = 0x000000009600004f
		x6             = 0x00000000fc8a4f90
		x7             = 0x00000000fc9ccc10
		x8             = 0x0000000001000000
		x9             = 0x0000000004035200
		x10            = 0x0000009fa226e8f3
		x11            = 0x0000021e648978a7
		x12            = 0x0000000000000002
		x13            = 0x0000000000000002
		x14            = 0x0000000000000001
		x15            = 0x00000000000000ff
		x16            = 0x00000000f73f9454
		x17            = 0x00000000fcb41000
		x18            = 0x00000000fc8a5130
		x19            = 0x0000000004035a70
		x20            = 0x0000000000000000
		x21            = 0x0000000000000000
		x22            = 0x0000000000000000
		x23            = 0x0000000000000000
		x24            = 0x0000000000000000
		x25            = 0x0000000000000000
		x26            = 0x0000000000000000
		x27            = 0x0000000000000000
		x28            = 0x0000000000000000
		x29            = 0x0000000000000000
		scr_el3        = 0x000000401c07073d
		sctlr_el3      = 0x00000000b0cd183f
		cptr_el3       = 0x0000000000000000
		tcr_el3        = 0x000000008081351c
		daif           = 0x00000000000003c0
		mair_el3       = 0x00000000004404ff
		spsr_el3       = 0x00000000630002cd
		elr_el3        = 0x0000000004021c30
		ttbr0_el3      = 0x000000000403c001
		esr_el3        = 0x00000000be000211
		far_el3        = 0x0000000000000000
		spsr_el1       = 0x0000000000000000
		elr_el1        = 0x0000000000000000
		spsr_abt       = 0x0000000000000000
		spsr_und       = 0x0000000000000000
		spsr_irq       = 0x0000000000000000
		spsr_fiq       = 0x0000000000000000
		sctlr_el1      = 0x0000000030d00980
		actlr_el1      = 0x0000000000000000
		cpacr_el1      = 0x0000000000300000
		csselr_el1     = 0x0000000000000004
		sp_el1         = 0x0000000000000000
		esr_el1        = 0x0000000000000000
		ttbr0_el1      = 0x0000000000000000
		ttbr1_el1      = 0x0000000000000000
		mair_el1       = 0x0000000000000000
		amair_el1      = 0x0000000000000000
		tcr_el1        = 0x0000000000000000
		tpidr_el1      = 0x0000000000000000
		tpidr_el0      = 0x00000000f501edc0
		tpidrro_el0    = 0x0000000000000000
		par_el1        = 0x0000000000000800
		mpidr_el1      = 0x0000000081000000
		afsr0_el1      = 0x0000000000000000
		afsr1_el1      = 0x0000000000000000
		contextidr_el1 = 0x0000000000000000
		vbar_el1       = 0x0000000000000000
		cntp_ctl_el0   = 0x0000000000000002
		cntp_cval_el0  = 0x00000020f49a0984
		cntv_ctl_el0   = 0x0000000000000002
		cntv_cval_el0  = 0x00000020ecda7b2f
		cntkctl_el1    = 0x0000000000000000
		sp_el0         = 0x00000000fc8a5130
		isr_el1        = 0x0000000000000040
		cpuectlr_el1   = 0x0000000000900000

#. Upon running the tf-a-tests, a panic in EL3 is experienced, and some dump information will be presented on the ``FVP terminal_uart_ap`` window, similar to the following excerpt:

	::

		(...)
                Running test suite 'Framework Validation'
                Description: Validate the core features of the test framework> Executing 'NVM support'
                  TEST COMPLETE                                                 Passed> Executing 'NVM serialisation'
                  TEST COMPLETE                                                 Passed> Executing 'Events API'
                ERROR:   Unexpected affinity info state.
                BACKTRACE: START: psci_warmboot_entrypoint
                0: EL3: 0x4022478
                1: EL3: 0x4029aa0
                2: EL3: 0x40201d4
                3: EL3: 0x4022550
                BACKTRACE: END: psci_warmboot_entrypoint
                PANIC in EL3.
                x30            = 0x0000000004029aac
                x0             = 0x0000000000000001
                x1             = 0x0000000000000002
                x2             = 0x0000000000000097
                x3             = 0x00000000ffffffc8
                x4             = 0x0000000000000004
                x5             = 0x0000000000000000
                x6             = 0x000000000402ab70
                x7             = 0x0000000000000000
                x8             = 0x0000000001000000
                x9             = 0x00000000040201c8
                x10            = 0x0000000004021cf4
                x11            = 0x00000000fd030000
                x12            = 0x00000000fd000800
                x13            = 0x0000000080000000
                x14            = 0x00000000410fd801
                x15            = 0x0000000080000000
                x16            = 0x0000000000000000
                x17            = 0x0000000000000017
                x18            = 0x000000401c070f38
                x19            = 0x0000000004020140
                x20            = 0x0000000000000000
                x21            = 0x000000000403b000
                x22            = 0x0000000000000000
                x23            = 0x0000000000000000
                x24            = 0x0000000000000000
                x25            = 0x0000000000000000
                x26            = 0x0000000000000000
                x27            = 0x0000000000000000
                x28            = 0x0000000000000000
                x29            = 0x0000000004030b90
                scr_el3        = 0x0000000000030638
                sctlr_el3      = 0x00000000b0cd183f
                cptr_el3       = 0x0000000040100000
                tcr_el3        = 0x000000008081351c
                daif           = 0x00000000000002c0
                mair_el3       = 0x00000000004404ff
                spsr_el3       = 0x00000000604003c9
                elr_el3        = 0x00000000fd0013d0
                ttbr0_el3      = 0x000000000403c001
                esr_el3        = 0x0000000000000000
                far_el3        = 0x0000000000000000
                spsr_el1       = 0x0000000000000000
                elr_el1        = 0x0000000000000000
                spsr_abt       = 0x0000000000000000
                spsr_und       = 0x0000000000000000
                spsr_irq       = 0x0000000000000000
                spsr_fiq       = 0x0000000000000000
                sctlr_el1      = 0x0000000030d80998
                actlr_el1      = 0x0000000000000000
                cpacr_el1      = 0x0000000000000000
                csselr_el1     = 0x0000000000000000
                sp_el1         = 0x0000000000000000
                esr_el1        = 0x0000000000000000
                ttbr0_el1      = 0x0000000000000000
                ttbr1_el1      = 0x0000000000000000
                mair_el1       = 0x0000000000000000
                amair_el1      = 0x0000000000000000
                tcr_el1        = 0x0000000080000000
                tpidr_el1      = 0x0000000000000000
                tpidr_el0      = 0x0000000000000000
                tpidrro_el0    = 0x0000000000000000
                par_el1        = 0xff0000000402a980
                mpidr_el1      = 0x0000000081000000
                afsr0_el1      = 0x0000000000000000
                afsr1_el1      = 0x0000000000000000
                contextidr_el1 = 0x0000000000000000
                vbar_el1       = 0x0000000000000000
                cntp_ctl_el0   = 0x0000000000000000
                cntp_cval_el0  = 0x0000000000000000
                cntv_ctl_el0   = 0x0000000000000000
                cntv_cval_el0  = 0x0000000000000000
                cntkctl_el1    = 0x0000000000000000
                sp_el0         = 0x0000000004030b90
                isr_el1        = 0x0000000000000000
                cpuectlr_el1   = 0x0000000000900000


Support
-------
For support email: support@arm.com.


--------------

*Copyright (c) 2022-2024, Arm Limited. All rights reserved.*
