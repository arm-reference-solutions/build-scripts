.. _docs/totalcompute/tc2/tc2_sw_stack:


Total Compute Platform Software Components
==========================================

.. figure:: tc2_sw_stack.svg
   :alt: Total Compute Software Stack

RSS Firmware
------------
Runtime Security Subsystem (RSS) serves as the Root of Trust for the Total Compute platform.
	
RSS BL1 code is the first software that executes right after a cold reset or Power-on.
	
RSS initially boots from immutable code (BL1_1) in its internal ROM, before jumping to BL1_2, which is provisioned and hash-locked in RSS OTP.
The updatable MCUboot BL2 boot stage is loaded from the flash into RSS SRAM, where it is authenticated.
BL2 loads and authenticates the TF-M runtime into RSS SRAM from host flash. BL2 is also responsible for loading initial boot code into other subsystems within Total Compute as below.

 #. SCP BL1;
 #. AP BL1.

The following diagram illustrates the boot flow sequence:

.. figure:: tc2_bootflow.svg
   :alt: Total Compute boot flow sequence


SCP Firmware
------------
The System Control Processor (SCP) is a compute unit of Total Compute and is responsible for low-level system management. The SCP is a Cortex-M3 processor with a set of dedicated peripherals and interfaces that you can extend.
SCP firmware supports:

 #. Power-up sequence and system start-up;
 #. Initial hardware configuration;
 #. Clock management;
 #. Servicing power state requests from the OS Power Management (OSPM) software.

SCP BL1
........
It performs the following functions:

 #. Sets up generic timer, UART console and clocks;
 #. Initializes the Coherent Interconnect;
 #. Powers ON primary AP CPU;
 #. Loads SCP Runtime Firmware.

SCP Runtime Firmware
....................
SCP runtime code starts execution after TF-A BL2 has authenticated and copied it from flash.
It performs the following functions:

 #. Responds to SCMI messages via MHUv2 for CPU power control and DVFS;
 #. Power Domain management;
 #. Clock management.

AP Secure World Software
------------------------
Secure software/firmware is a trusted software component that runs in the AP secure world. It mainly consists of AP firmware, Secure Partition Manager and Secure Partitions (OP-TEE, Trusted Services).

AP firmware
...........
The AP firmware consists of the code that is required to boot Total Compute platform up to the point where the OS execution starts. This firmware performs architecture and platform initialization. It also loads and initializes secure world images like Secure partition manager and Trusted OS.

Trusted Firmware-A (TF-A) BL1
+++++++++++++++++++++++++++++
BL1 performs minimal architectural initialization (like exception vectors, CPU initialization) and Platform initialization. It loads the BL2 image and passes control to it.

Trusted Firmware-A (TF-A) BL2
+++++++++++++++++++++++++++++
BL2 runs at S-EL1 and performs architectural initialization required for subsequent stages of TF-A and normal world software. It configures the TrustZone Controller and carves out memory region in DRAM for secure and non-secure use. BL2 loads below images:

 #. SCP BL2 image;
 #. EL3 Runtime Software (BL31 image);
 #. Secure Partition Manager (BL32 image);
 #. Non-Trusted firmware - EDK2 (BL33 image) and Grub;
 #. Secure Partitions images (OP-TEE and Trusted Services).

Trusted Firmware-A (TF-A) BL31
++++++++++++++++++++++++++++++
BL2 loads EL3 Runtime Software (BL31) and BL1 passes control to BL31 at EL3. In Total Compute BL31 runs at trusted SRAM. It provides the below mentioned runtime services:

 #. Power State Coordination Interface (PSCI);
 #. Secure Monitor framework;
 #. Secure Partition Manager Dispatcher.

Secure Partition Manager
........................
Total Compute enables FEAT S-EL2 architectural extension, and it uses Hafnium as Secure Partition Manager Core (SPMC). BL32 option in TF-A is re-purposed to specify the SPMC image. The SPMC component runs at S-EL2 exception level.

Secure Partitions
.................
Software image isolated using SPM is Secure Partition. Total Compute enables OP-TEE and Trusted Services as Secure Partitions.

OP-TEE
++++++
OP-TEE Trusted OS is virtualized using Hafnium at S-EL2. OP-TEE OS for Total Compute is built with FF-A and SEL2 SPMC support. This enables OP-TEE as a Secure Partition running in an isolated address space managed by Hafnium. The OP-TEE kernel runs at S-EL1 with Trusted applications running at S-EL0.

Trusted Services
++++++++++++++++
Trusted Services like Crypto Service, Internal Trusted Storage runs as S-EL0 Secure Partitions.

AP Non-Secure World Software
----------------------------

EDK2 (BL33)
...........
TF-A BL31 passes execution control to EDK2 UEFI FW bootloader (BL33). EDK2 UEFI FW will load and verify signature of Grub.


Grub bootloader
...............
Grub bootloader provides flexibility to configure some boot parameters, and ultimately loads and boots the Linux Kernel.


Linux Kernel
............
Linux Kernel in Total Compute contains the subsystem-specific features that demonstrate the capabilities of Total Compute.

Debian
......
This variant is based on the Debian 12 (aka Bookworm) filesystem. This image can be used for development or validation work that does not imply pixel rendering, as currently there is no support for software or hardware rendering.


--------------

*Copyright (c) 2022-2024, Arm Limited. All rights reserved.*
